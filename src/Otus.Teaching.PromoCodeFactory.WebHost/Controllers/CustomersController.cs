﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _repository;

        public CustomersController(IRepository<Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение списка клиентов.
        /// </summary>
        /// <returns>Список клиентов.</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _repository.GetAllAsync();
            var customerShortResponse = customers.Select(c => new CustomerShortResponse()
            {
                Id = c.Id,
                LastName = c.LastName,
                FirstName = c.FirstName,
                Email = c.Email
            });
            return Ok(customerShortResponse);
        }

        /// <summary>
        /// Получение клиента вместе с выданными ему промокодами.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <returns>Клиент с выданными ему промокодами.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);
            var customerResponse = new CustomerResponse
            {
                Id = customer.Id,
                LastName = customer.LastName,
                FirstName = customer.FirstName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse
                {
                    Id = p.Id,
                    Code = p.Code,
                    BeginDate = p.BeginDate.ToString(CultureInfo.InvariantCulture),
                    EndDate = p.EndDate.ToString(CultureInfo.InvariantCulture),
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                }).ToList(),
                Preferences = customer.Preferences.Select(p => new PreferenceShortResponse
                {
                    Id = p.Id,
                    Name = p.Name
                }).ToList()
            };
            return Ok(customerResponse);
        }

        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="request">Новый клиент с предпочтениями.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                LastName = request.LastName,
                FirstName = request.FirstName,
                Email = request.Email,
                Preferences = request.PreferenceIds.Select(id => new Preference
                {
                    Id = id
                }).ToList()
            };
            await _repository.CreateAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Обновление данных клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <param name="request">Измененный клиент с предпочтениями.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Id = id,
                LastName = request.LastName,
                FirstName = request.FirstName,
                Email = request.Email,
                Preferences = request.PreferenceIds.Select(prefId => new Preference
                {
                    Id = prefId
                }).ToList()
            };
            await _repository.EditAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _repository.DeleteAsync(new Customer { Id = id });
            return Ok();
        }
    }
}