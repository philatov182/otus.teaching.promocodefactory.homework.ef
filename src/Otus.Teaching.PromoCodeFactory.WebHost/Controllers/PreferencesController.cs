﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private IRepository<Preference> _repository;

        public PreferencesController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение списка предпочтений..
        /// </summary>
        /// <returns>Список предпочтений.</returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceShortResponse>> GetPreferencesAsync()
        {
            var preferences = await _repository.GetAllAsync();
            var preferenceShortResponse = preferences.Select(p => new PreferenceShortResponse()
            {
                Id = p.Id,
                Name = p.Name,
            });
            return Ok(preferenceShortResponse);
        }
    }
}
