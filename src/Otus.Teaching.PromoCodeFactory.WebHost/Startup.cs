using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var contextOptions = new DbContextOptionsBuilder<PromoCodeFactoryContext>()
                .UseSqlite("Filename=temp.db")
                .Options;

            services.AddTransient(typeof(IRepository<Role>), (x) =>
                new EfRepository<Role>(new PromoCodeFactoryContext(contextOptions)));
            services.AddTransient(typeof(IRepository<Employee>), (x) =>
                new EfRepository<Employee>(new PromoCodeFactoryContext(contextOptions)));
            services.AddTransient(typeof(IRepository<Preference>), (x) =>
                new EfRepository<Preference>(new PromoCodeFactoryContext(contextOptions)));
            services.AddTransient(typeof(IRepository<PromoCode>), (x) =>
                new PromoCodeRepository(new PromoCodeFactoryContext(contextOptions)));
            services.AddTransient(typeof(IRepository<Customer>), (x) =>
                new CustomerRepository(new PromoCodeFactoryContext(contextOptions)));


            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}